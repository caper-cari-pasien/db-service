package com.caper.dbservice.controller;

import lombok.RequiredArgsConstructor;

import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.caper.dbservice.dto.TreatmentChatDto;
import com.caper.dbservice.model.TreatmentChat;
import com.caper.dbservice.service.TreatmentChatService;


@RestController
@RequestMapping("/api/treatment/chat")
@RequiredArgsConstructor
public class TreatmentChatController {

    private final TreatmentChatService treatmentChatService;
    
    @PostMapping("")
    public ResponseEntity<TreatmentChat> sendTreatmentChat(@RequestBody TreatmentChatDto treatmentChatDto) {
        var response = treatmentChatService.sendTreatmentChat(treatmentChatDto);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{treatmentId}")
    public ResponseEntity<List<TreatmentChat>> getChatByTreatmentId(@PathVariable("treatmentId") String treatmentId) {
        List<TreatmentChat> treatmentChats = treatmentChatService.getChatByTreatmentId(treatmentId);
        return ResponseEntity.ok(treatmentChats);
    }

}
