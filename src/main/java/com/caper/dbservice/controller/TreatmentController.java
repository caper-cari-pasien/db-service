package com.caper.dbservice.controller;

import com.caper.dbservice.dto.OpenTreatmentDto;
import com.caper.dbservice.dto.TreatmentCategoryDto;
import com.caper.dbservice.model.OpenTreatment;
import com.caper.dbservice.model.TreatmentCategory;
import com.caper.dbservice.service.TreatmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/treatment")
@RequiredArgsConstructor
public class TreatmentController {
    private final TreatmentService treatmentService;

    @GetMapping("")
    public ResponseEntity<List<OpenTreatment>> getTreatmentAll() {
        return ResponseEntity.ok(treatmentService.getTreatmentAll());
    }

    @PostMapping("")
    public ResponseEntity<OpenTreatment> add(@RequestBody OpenTreatmentDto openTreatmentDto) {
        return ResponseEntity.ok(treatmentService.addOpenTreatment(openTreatmentDto));
    }

    @GetMapping("/{id}")
    public ResponseEntity<OpenTreatment> getTreatmentById(@PathVariable String id) {
        return ResponseEntity.ok(treatmentService.getTreatmentById(id));
    }

    @PostMapping("/category")
    public ResponseEntity<TreatmentCategory> addTreatmentCategory(@RequestBody TreatmentCategoryDto treatmentCategoryDto) {
        return ResponseEntity.ok(treatmentService.addTreatmentCategory(treatmentCategoryDto));
    }
    @GetMapping("/category")
    public ResponseEntity<List<TreatmentCategory>> getTreatmentCategoryAll() {
        return ResponseEntity.ok(treatmentService.getAllCategory());
    }
    @GetMapping("/category/{id}")
    public ResponseEntity<TreatmentCategory> getCategoryById(@PathVariable String id) {
        return ResponseEntity.ok(treatmentService.getCategoryById(id));
    }
}
