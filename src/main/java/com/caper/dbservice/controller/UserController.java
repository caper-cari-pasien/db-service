package com.caper.dbservice.controller;

import com.caper.dbservice.dto.DoctorDto;
import com.caper.dbservice.dto.PatientDto;
import com.caper.dbservice.dto.UploadImageUrlDto;
import com.caper.dbservice.model.Doctor;
import com.caper.dbservice.model.Patient;
import com.caper.dbservice.model.User;
import com.caper.dbservice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/id/{id}")
    public ResponseEntity<User> getById(@PathVariable String id) {
        return ResponseEntity.ok(userService.getUserById(id));
    }

    @GetMapping("/name/{username}")
    public ResponseEntity<User> getByName(@PathVariable String username) {
        return ResponseEntity.ok(userService.getUserByUsername(username));
    }

    @PostMapping("/add")
    public ResponseEntity<User> add(@RequestBody User user){
        return ResponseEntity.ok(userService.addUser(user));
    }

    @GetMapping("/profile/dokter/all")
    public ResponseEntity<List<Doctor>> getDoctorAll() {
        return ResponseEntity.ok(userService.getDoctorAll());
    }

    @GetMapping("/profile/dokter/{username}")
    public ResponseEntity<Doctor> getDoctorByName(@PathVariable String username) {
        return ResponseEntity.ok(userService.getDoctorByUsername(username));
    }

    @GetMapping("/profile/pasien/{username}")
    public ResponseEntity<Patient> getPatientByName(@PathVariable String username) {
        return ResponseEntity.ok(userService.getPatientByUsername(username));
    }

    @PutMapping("/profile/dokter/update/{username}")
    public ResponseEntity<Doctor> updateDoctor(@PathVariable String username, @RequestBody DoctorDto doctor) {
        return ResponseEntity.ok(userService.updateDoctor(username, doctor));
    }

    @PutMapping("/profile/pasien/update/{username}")
    public ResponseEntity<Patient> updatePatient(@PathVariable String username, @RequestBody PatientDto patient) {
        return ResponseEntity.ok(userService.updatePatient(username, patient));
    }

    @PatchMapping("/profile/dokter/certificate/{username}")
    public ResponseEntity<Doctor> uploadCertificate(@PathVariable String username, @RequestBody UploadImageUrlDto uploadImageUrlDto) {
        return ResponseEntity.ok(userService.uploadCertificate(username, uploadImageUrlDto));
    }

    @PatchMapping("/profile/pasien/teeth-picture/{username}")
    public ResponseEntity<Patient> uploadTeethPicture(@PathVariable String username, @RequestBody UploadImageUrlDto uploadImageUrlDto) {
        return ResponseEntity.ok(userService.uploadTeethPicture(username, uploadImageUrlDto));
    }
}
