package com.caper.dbservice.controller;

import com.caper.dbservice.dto.RequestTreatmentDto;
import com.caper.dbservice.model.RequestTreatment;
import com.caper.dbservice.service.RequestTreatmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/treatment/pesan")
@RequiredArgsConstructor
public class RequestTreatmentController {
    private final RequestTreatmentService requestTreatmentService;

    @GetMapping("")
    public ResponseEntity<List<RequestTreatment>> findAllTreatment() {
        return ResponseEntity.ok(requestTreatmentService.findAllTreatment());
    }

    @PostMapping("")
    public ResponseEntity<RequestTreatment> addRequest(@RequestBody RequestTreatmentDto requestTreatmentDto) {
        var responseBody = requestTreatmentService.addRequest(requestTreatmentDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(responseBody);
    }
    @GetMapping("/{id}")
    public ResponseEntity<RequestTreatment> getRequestById(@PathVariable String id) {
        return ResponseEntity.ok(requestTreatmentService.getRequestById(id));
    }
}
