package com.caper.dbservice.controller;

import com.caper.dbservice.dto.AddTreatmentNoteDto;
import com.caper.dbservice.dto.RegisterTreatmentDto;
import com.caper.dbservice.model.Treatment;
import com.caper.dbservice.service.TreatmentBerjalanService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/treatment/berjalan")
@RequiredArgsConstructor
public class TreatmentBerjalanController {
    private final TreatmentBerjalanService treatmentBerjalanService;

    @GetMapping("/patient/{username}")
    public ResponseEntity<List<Treatment>> findTreatmentByPatientUsername(@PathVariable("username") String username) {
        var treatments = treatmentBerjalanService.findAllTreatmentByPatientUsername(username);
        return ResponseEntity.ok(treatments);
    }

    @GetMapping("/doctor/{username}")
    public ResponseEntity<List<Treatment>> findTreatmentByDoctorUsername(@PathVariable("username") String username) {
        var treatments = treatmentBerjalanService.findAllTreatmentByDoctorUsername(username);
        return ResponseEntity.ok(treatments);
    }

    @PatchMapping("verify/{treatmentId}")
    public ResponseEntity<Treatment> verifyTreatment(@PathVariable("treatmentId") String treatmentId) {
        var treatment = treatmentBerjalanService.verifyTreatment(treatmentId);
        return ResponseEntity.ok(treatment);
    }

    @PatchMapping("/{treatmentId}/note")
    public ResponseEntity<Treatment> addNote(@PathVariable("treatmentId") String treatmentId, @RequestBody AddTreatmentNoteDto treatmentNoteDto) {
        var treatment = treatmentBerjalanService.addNote(treatmentId, treatmentNoteDto);
        return ResponseEntity.ok(treatment);
    }

    @PostMapping("")
    public ResponseEntity<Treatment> registerTreatment(@RequestBody RegisterTreatmentDto registerTreatmentDto) {
        var treatment = treatmentBerjalanService.registerTreatment(registerTreatmentDto);
        return ResponseEntity.ok(treatment);
    }

    @GetMapping("/{treatmentId}")
    public ResponseEntity<Treatment> getTreatmentBerjalanById(@PathVariable String treatmentId) {
        return ResponseEntity.ok(treatmentBerjalanService.getTreatmentBerjalanById(treatmentId));
    }
}
