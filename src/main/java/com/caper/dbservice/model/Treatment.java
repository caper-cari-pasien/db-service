package com.caper.dbservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Treatment")
public class Treatment {
    @Id
    @Column(name = "id", unique = true)
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @Column(name = "note")
    private String note;

    @Column(name = "isVerified", columnDefinition = "boolean default false", nullable = false)
    private Boolean isVerified;

    @Column(name = "startTime", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;

    @ManyToOne
    @JsonManagedReference
    @JoinColumn(name = "openTreatmentId", referencedColumnName = "id", nullable = false)
    private OpenTreatment openTreatment;

    @ManyToOne
    @JsonManagedReference
    @JoinColumn(name = "patientId", referencedColumnName = "id", nullable = false)
    private Patient patient;

    @OneToOne(mappedBy = "treatment")
    @JsonManagedReference
    private TreatmentChatRoom chatRoom;
}
