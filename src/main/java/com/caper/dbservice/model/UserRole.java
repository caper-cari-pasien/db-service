package com.caper.dbservice.model;

public enum UserRole {
    PATIENT, DOCTOR
}
