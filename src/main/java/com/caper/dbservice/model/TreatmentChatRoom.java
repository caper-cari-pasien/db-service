package com.caper.dbservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "TreatmentChatRoom")
public class TreatmentChatRoom {
    @Id
    @Column(name = "id", unique = true)
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @OneToOne
    @JsonBackReference
    @JoinColumn(name = "treatmentId", referencedColumnName = "id", nullable = false)
    private Treatment treatment;

    
}
