package com.caper.dbservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Patient")
public class Patient {
    @Id
    @Column(unique = true, name="id")
    private String id;

    @OneToOne
    @MapsId
    @JoinColumn(name = "id")
    private User user;

    @Column(name = "teethPictureUrl")
    private String teethPictureUrl;

    @OneToMany(targetEntity = RequestTreatment.class, mappedBy = "patient", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonBackReference
    private List<RequestTreatment> requestTreatmentList;

    @OneToMany(targetEntity = Treatment.class, mappedBy = "patient", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonBackReference
    private List<Treatment> treatmentList;
}
