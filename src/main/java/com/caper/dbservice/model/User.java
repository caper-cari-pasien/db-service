package com.caper.dbservice.model;

import jakarta.persistence.*;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_User")
public class User {
    @Id
    @Column(unique = true, name = "id")
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @Column(name = "nama", nullable = false)
    private String nama;

    @Enumerated(EnumType.STRING)
    @Column(name = "role", nullable = false)
    private UserRole role;

    @Column(name = "domisili", nullable = false)
    private String domisili;

    @Column(name = "umur", nullable = false)
    private int umur;

    @Column(name = "password", nullable = false)
    private String password;
}
