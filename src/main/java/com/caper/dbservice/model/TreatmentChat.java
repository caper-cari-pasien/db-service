package com.caper.dbservice.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "TreatmentChat")
public class TreatmentChat {
    @Id
    @Column(name = "id", unique = true)
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @Column(name = "content", nullable = false)
    private String content;

    @Column(name = "time", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date time;

    @ManyToOne
    @JsonManagedReference
    @JoinColumn(name = "treatmentId", referencedColumnName = "id", nullable = false)
    private Treatment treatment;

    @ManyToOne
    @JsonManagedReference
    @JoinColumn(name = "treatmentChatRoomId", referencedColumnName = "id", nullable = false)
    private TreatmentChatRoom chatRoom;

    @ManyToOne
    @JsonManagedReference
    @JoinColumn(name = "senderId", referencedColumnName = "id", nullable = false)
    private User user;
}
