package com.caper.dbservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "RequestTreatment")
public class RequestTreatment {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "location", nullable = false)
    private String location;

    @Column(name = "treatmentTime", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date treatmentTime;

    @Column(name = "description", nullable = false)
    private String description;

    @ManyToOne
    @JsonManagedReference
    @JoinColumn(name = "patientId", referencedColumnName = "id", nullable = false)
    private Patient patient;
}
