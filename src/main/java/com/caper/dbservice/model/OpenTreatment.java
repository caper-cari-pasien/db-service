package com.caper.dbservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "OpenTreatment")
public class OpenTreatment {
    @Id
    @Column(name = "id", unique = true)
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "location", nullable = false)
    private String location;

    @Column(name = "treatmentTime", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date treatmentTime;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "price", nullable = false)
    private Long price;

    @ManyToOne
    @JsonManagedReference
    @JoinColumn(name = "categoryId", referencedColumnName = "id", nullable = false)
    private TreatmentCategory category;

    @ManyToOne
    @JsonManagedReference
    @JoinColumn(name = "doctorId", referencedColumnName = "id", nullable = false)
    private Doctor doctor;

    @OneToMany(targetEntity = Treatment.class, mappedBy = "openTreatment", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonBackReference
    private List<Treatment> treatmentList;
}
