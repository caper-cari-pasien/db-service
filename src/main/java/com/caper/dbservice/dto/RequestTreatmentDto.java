package com.caper.dbservice.dto;

import com.caper.dbservice.model.Patient;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RequestTreatmentDto {
    private String title;

    private String location;

    private Date treatmentTime;

    private String description;

    private String patientUsername;

}
