package com.caper.dbservice.dto;

import com.caper.dbservice.model.Patient;
import com.caper.dbservice.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PatientDto {
    private String id;
    private User user;
    private String teethPictureUrl;


    public PatientDto(Patient patient){
        this.id = patient.getId();
        this.user = patient.getUser();
        this.teethPictureUrl = patient.getTeethPictureUrl();
    }
}