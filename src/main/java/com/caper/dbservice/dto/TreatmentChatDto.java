package com.caper.dbservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TreatmentChatDto {
    private String content;
    private Date time;
    private String treatmentId;
    private String senderId;
}
