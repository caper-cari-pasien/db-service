package com.caper.dbservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OpenTreatmentDto {
    private String location;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date treatmentTime;
    private String description;
    private Long price;
    private String categoryId;
    private String doctorUsername;
    private String title;
}
