package com.caper.dbservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;
import java.util.List;
import org.springframework.format.annotation.DateTimeFormat;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TreatmentChatRoomDto {
    private String id;
    private TreatmentDto treatment;
    private List<TreatmentChatDto> chatList;
}
