package com.caper.dbservice.dto;

import java.util.Date;
import com.caper.dbservice.model.OpenTreatment;
import com.caper.dbservice.model.Patient;
import com.caper.dbservice.model.TreatmentChatRoom;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TreatmentDto {
    private String id;
    private String note;
    private Boolean isVerified;
    private Date startTime;
    private OpenTreatment openTreatment;
    private Patient patient;
    private TreatmentChatRoom chatRoom;
}
