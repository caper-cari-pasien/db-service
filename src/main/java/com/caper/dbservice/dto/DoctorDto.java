package com.caper.dbservice.dto;

import com.caper.dbservice.model.Doctor;
import com.caper.dbservice.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DoctorDto {
    private String id;
    private User user;
    private String certificateUrl;


    public DoctorDto(Doctor doctor){
        this.id = doctor.getId();
        this.user = doctor.getUser();
        this.certificateUrl = doctor.getCertificateUrl();
    }
}