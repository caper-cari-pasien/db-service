package com.caper.dbservice.dto;

import com.caper.dbservice.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private String nama;
    private String username;
    private String password;
    private String role;
    private Integer umur;
    private String domisili;

    public UserDto(User user){
        this.nama = user.getNama();
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.role = user.getRole().toString();
        this.umur = user.getUmur();
        this.domisili = user.getDomisili();
    }
}