package com.caper.dbservice.service;

import lombok.RequiredArgsConstructor;

import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;
import com.caper.dbservice.dto.TreatmentChatDto;
import com.caper.dbservice.exceptions.DataNotFoundException;
import com.caper.dbservice.model.TreatmentChat;
import com.caper.dbservice.repository.TreatmentChatRepository;
import com.caper.dbservice.repository.TreatmentChatRoomRepository;
import com.caper.dbservice.repository.TreatmentRepository;
import com.caper.dbservice.repository.UserRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class TreatmentChatService {
    
    private final TreatmentChatRepository treatmentChatRepository;
    private final TreatmentRepository treatmentRepository;
    private final UserRepository userRepository;


    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public TreatmentChat sendTreatmentChat(TreatmentChatDto treatmentChatDto) {

        var treatment = treatmentRepository.findById(treatmentChatDto.getTreatmentId())
                                .orElseThrow(() -> new DataNotFoundException(String.format("Treatment with id %s  not found", 
                                treatmentChatDto.getTreatmentId())));
        var user = userRepository.findById(treatmentChatDto.getSenderId())
                                .orElseThrow(() -> new DataNotFoundException(String.format("User with id %s  not found", 
                                treatmentChatDto.getSenderId())));
        var time = new Date(System.currentTimeMillis());
        System.out.println(treatmentChatDto.getTreatmentId());
        System.out.println(treatmentChatDto.getSenderId());

        return treatmentChatRepository.save(
                TreatmentChat.builder()
                        .content(treatmentChatDto.getContent())
                        .time(time)
                        .treatment(treatment)
                        .chatRoom(treatment.getChatRoom())
                        .user(user)
                        .build()
        );
    }

    public List<TreatmentChat> getChatByTreatmentId(String treatmentId) {
        System.out.println(treatmentId);
        return treatmentChatRepository.findByTreatmentId(treatmentId);
    }

    
}
