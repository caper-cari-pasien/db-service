package com.caper.dbservice.service;

import com.caper.dbservice.dto.DoctorDto;
import com.caper.dbservice.dto.PatientDto;
import com.caper.dbservice.dto.UploadImageUrlDto;
import com.caper.dbservice.exceptions.DataNotFoundException;
import com.caper.dbservice.model.Doctor;
import com.caper.dbservice.model.Patient;
import com.caper.dbservice.model.User;
import com.caper.dbservice.model.UserRole;
import com.caper.dbservice.repository.DoctorRepository;
import com.caper.dbservice.repository.PatientRepository;
import com.caper.dbservice.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PatientRepository patientRepository;
    private final DoctorRepository doctorRepository;

    public User getUserById(String id) {
        Optional<User> user = userRepository.findById(id);

        if(user.isEmpty()) throw new DataNotFoundException(String.format("User with id %s not found", id));

        return user.get();
    }

    public User getUserByUsername(String username){
        Optional<User> user = userRepository.findByUsername(username);

        if(user.isEmpty()) throw new DataNotFoundException(String.format("User with name %s not found", username));

        return user.get();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public User addUser(User user){
        User newUser = userRepository.save(user);
        if(newUser.getRole().equals(UserRole.DOCTOR)){
            doctorRepository.save(Doctor.builder().user(newUser).build());
        }else{
            patientRepository.save(Patient.builder().user(newUser).build());
        }
        return newUser;
    }

    public List<Doctor> getDoctorAll() {
        List<Doctor> doctors = doctorRepository.findAll();
        if (doctors.isEmpty()) {
            throw new DataNotFoundException("There are no doctors at the moment");
        }

        return doctors;
    }

    public Doctor getDoctorByUsername(String username) {
        User user = this.getUserByUsername(username);

        Optional<Doctor> doctor = doctorRepository.findById(user.getId());

        if(doctor.isEmpty()) throw new DataNotFoundException(String.format("User with name %s not found", username));

        return doctor.get();
    }

    public Patient getPatientByUsername(String username) {
        User user = this.getUserByUsername(username);

        Optional<Patient> patient = patientRepository.findById(user.getId());

        if(patient.isEmpty()) throw new DataNotFoundException(String.format("User with name %s not found", username));

        return patient.get();
    }

    public Doctor updateDoctor(String username, DoctorDto doctor) {
        User user = this.getUserByUsername(username);

        Optional<Doctor> doctorOpt = doctorRepository.findById(user.getId());

        if(doctorOpt.isEmpty()) throw new DataNotFoundException(String.format("User with name %s not found", username));
        User userNew = updateUser(doctor.getUser());
        Doctor doctorNew = Doctor.builder()
                .id(user.getId())
                .user(userNew)
                .certificateUrl(doctorOpt.get().getCertificateUrl())
                .build();
        doctorRepository.save(doctorNew);
        return doctorNew;
    }

    public Patient updatePatient(String username, PatientDto patient) {
        User user = this.getUserByUsername(username);

        Optional<Patient> patientOpt = patientRepository.findById(user.getId());

        if(patientOpt.isEmpty()) throw new DataNotFoundException(String.format("User with name %s not found", username));
        User userNew = updateUser(patient.getUser());
        Patient patientNew = Patient.builder()
                .id(user.getId())
                .user(userNew)
                .teethPictureUrl(patientOpt.get().getTeethPictureUrl())
                .build();
        patientRepository.save(patientNew);
        return patientNew;
    }

    public User updateUser(User updateUser) {
        User user = this.getUserByUsername(updateUser.getUsername());
        User userNew = User.builder()
                .id(user.getId())
                .username(updateUser.getUsername())
                .nama(updateUser.getNama())
                .role(updateUser.getRole())
                .domisili(updateUser.getDomisili())
                .umur(updateUser.getUmur())
                .password(user.getPassword())
                .build();
        userRepository.save(userNew);
        return userNew;
    }

    public Doctor uploadCertificate(String username, UploadImageUrlDto uploadImageUrlDto) {
        Optional<Doctor> doctorOpt = doctorRepository.findByUserUsername(username);
        if(doctorOpt.isEmpty()) throw new DataNotFoundException(String.format("User with name %s not found", username));
        Doctor doctor = doctorOpt.get();
        doctor.setCertificateUrl(uploadImageUrlDto.getUrl());
        doctorRepository.save(doctor);
        return doctor;
    }

    public Patient uploadTeethPicture(String username, UploadImageUrlDto uploadImageUrlDto) {
        Optional<Patient> patientOpt = patientRepository.findByUserUsername(username);
        if(patientOpt.isEmpty()) throw new DataNotFoundException(String.format("User with name %s not found", username));
        Patient patient = patientOpt.get();
        patient.setTeethPictureUrl(uploadImageUrlDto.getUrl());
        patientRepository.save(patient);
        return patient;
    }
}
