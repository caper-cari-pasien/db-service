package com.caper.dbservice.service;

import com.caper.dbservice.dto.RequestTreatmentDto;
import com.caper.dbservice.exceptions.DataNotFoundException;
import com.caper.dbservice.model.OpenTreatment;
import com.caper.dbservice.model.RequestTreatment;
import com.caper.dbservice.repository.PatientRepository;
import com.caper.dbservice.repository.RequestTreatmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RequestTreatmentService {
    private final RequestTreatmentRepository requestTreatmentRepository;
    private final PatientRepository patientRepository;

    public List<RequestTreatment> findAllTreatment() {
        return requestTreatmentRepository.findAll();
    }
    public RequestTreatment getRequestById(String id) {
        Optional<RequestTreatment> requestTreatment = requestTreatmentRepository.findById(id);

        if (requestTreatment.isEmpty()) throw new DataNotFoundException(String.format("Request with id %s not found", id));

        return requestTreatment.get();
    }
    public RequestTreatment addRequest(RequestTreatmentDto requestTreatmentDto) {
        var patient = patientRepository.findByUserUsername(requestTreatmentDto.getPatientUsername());

        if(patient.isEmpty()){
            throw new DataNotFoundException(String.format("Patient %s not found", requestTreatmentDto.getPatientUsername()));
        }

        var request = RequestTreatment.builder().title(requestTreatmentDto.getTitle())
                .patient(patient.get())
                .treatmentTime(requestTreatmentDto.getTreatmentTime())
                .description(requestTreatmentDto.getDescription())
                .location(requestTreatmentDto.getLocation())
                .build();

        return requestTreatmentRepository.save(request);
    }
}
