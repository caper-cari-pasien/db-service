package com.caper.dbservice.service;

import com.caper.dbservice.dto.AddTreatmentNoteDto;
import com.caper.dbservice.dto.RegisterTreatmentDto;
import com.caper.dbservice.exceptions.DataNotFoundException;
import com.caper.dbservice.model.RequestTreatment;
import com.caper.dbservice.model.Treatment;
import com.caper.dbservice.model.TreatmentChatRoom;
import com.caper.dbservice.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TreatmentBerjalanService {
    private final PatientRepository patientRepository;
    private final DoctorRepository doctorRepository;
    private final TreatmentRepository treatmentRepository;
    private final OpenTreatmentRepository openTreatmentRepository;
    private final TreatmentChatRoomRepository treatmentChatRoomRepository;

    public List<Treatment> findAllTreatmentByPatientUsername(String username){
        var patient = patientRepository.findByUserUsername(username);

        if(patient.isEmpty()){
            throw new DataNotFoundException(String.format("Patient %s not found", username));
        }

        return treatmentRepository.findAllByPatient_Id(patient.get().getId());
    }

    public List<Treatment> findAllTreatmentByDoctorUsername(String username){
        var doctor = doctorRepository.findByUserUsername(username);

        if(doctor.isEmpty()){
            throw new DataNotFoundException(String.format("Doctor %s not found", username));
        }

        return treatmentRepository.findAllByOpenTreatment_Doctor_Id(doctor.get().getId());
    }
    public Treatment getTreatmentBerjalanById(String id) {
        Optional<Treatment> treatment = treatmentRepository.findById(id);

        if (treatment.isEmpty()) throw new DataNotFoundException(String.format("Treatment Ongoing with id %s not found", id));

        return treatment.get();
    }
    public Treatment verifyTreatment(String treatmentId) {
        var treatment = treatmentRepository.findById(treatmentId);

        if(treatment.isEmpty()) {
            throw new DataNotFoundException(String.format("Treatment %s not found", treatmentId));
        }

        var treatmentVerify = treatment.get();
        treatmentVerify.setIsVerified(true);

        return treatmentRepository.save(treatmentVerify);
    }

    public Treatment addNote(String treatmentId, AddTreatmentNoteDto note) {
        var treatment = treatmentRepository.findById(treatmentId);

        if(treatment.isEmpty()) {
            throw new DataNotFoundException(String.format("Treatment %s not found", treatmentId));
        }

        var treatmentPatched = treatment.get();
        treatmentPatched.setNote(note.getNote());

        return treatmentRepository.save(treatmentPatched);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public Treatment registerTreatment(RegisterTreatmentDto registerTreatmentDto) {
        var patient = patientRepository.findByUserUsername(registerTreatmentDto.getUsername());

        if(patient.isEmpty()){
            throw new DataNotFoundException("Patient not found");
        }

        var openTreatment = openTreatmentRepository.findById(registerTreatmentDto.getOpenTreatmentId());

        if(openTreatment.isEmpty()) {
            throw new DataNotFoundException("Open Treatment not found");
        }

        var treatment = Treatment.builder().openTreatment(openTreatment.get())
                .patient(patient.get())
                .note("")
                .isVerified(false)
                .startTime(registerTreatmentDto.getStartTime())
                .build();
        var chatRoom = TreatmentChatRoom.builder().treatment(treatment).build();
        treatment.setChatRoom(chatRoom);
        treatmentRepository.save(treatment);
        treatmentChatRoomRepository.save(chatRoom);

        return treatment;
    }
}
