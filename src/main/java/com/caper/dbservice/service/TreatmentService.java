package com.caper.dbservice.service;

import com.caper.dbservice.dto.OpenTreatmentDto;
import com.caper.dbservice.dto.TreatmentCategoryDto;
import com.caper.dbservice.exceptions.DataNotFoundException;
import com.caper.dbservice.model.*;
import com.caper.dbservice.repository.DoctorRepository;
import com.caper.dbservice.repository.RequestTreatmentRepository;
import com.caper.dbservice.repository.OpenTreatmentRepository;
import com.caper.dbservice.repository.TreatmentCategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TreatmentService {
    private final OpenTreatmentRepository openTreatmentRepository;
    private final RequestTreatmentRepository requestTreatmentRepository;
    private final TreatmentCategoryRepository treatmentCategoryRepository;
    private final DoctorRepository doctorRepository;

    public List<OpenTreatment> getTreatmentAll() {
        return openTreatmentRepository.findAll();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public OpenTreatment addOpenTreatment(OpenTreatmentDto openTreatmentDto) {

        Optional<TreatmentCategory> treatmentCategoryOptional = treatmentCategoryRepository.findById(openTreatmentDto.getCategoryId());
        if (treatmentCategoryOptional.isEmpty()) throw new DataNotFoundException(String.format("Category with id %s not found", openTreatmentDto.getCategoryId()));

        Optional<Doctor> doctor = doctorRepository.findByUserUsername(openTreatmentDto.getDoctorUsername());
        if (doctor.isEmpty()) throw new DataNotFoundException(String.format("Doctor %s not found", openTreatmentDto.getDoctorUsername()));

        return openTreatmentRepository.save(
                OpenTreatment.builder()
                        .location(openTreatmentDto.getLocation())
                        .treatmentTime(openTreatmentDto.getTreatmentTime())
                        .description(openTreatmentDto.getDescription())
                        .price(openTreatmentDto.getPrice())
                        .category(treatmentCategoryOptional.get())
                        .doctor(doctor.get())
                        .title(openTreatmentDto.getTitle())
                        .build()
        );
    }

    public OpenTreatment getTreatmentById(String id) {
        Optional<OpenTreatment> openTreatment = openTreatmentRepository.findById(id);

        if (openTreatment.isEmpty()) throw new DataNotFoundException(String.format("Treatment with id %s not found", id));

        return openTreatment.get();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public TreatmentCategory addTreatmentCategory(TreatmentCategoryDto treatmentCategoryDto) {
        return treatmentCategoryRepository.save(
                TreatmentCategory.builder()
                        .name(treatmentCategoryDto.getName())
                        .build()
        );
    }

    public TreatmentCategory getCategoryById(String id) {
        Optional<TreatmentCategory> treatmentCategoryOptional = treatmentCategoryRepository.findById(id);

        if (treatmentCategoryOptional.isEmpty()) throw new DataNotFoundException(String.format("Category with id %s not found", id));

        return treatmentCategoryOptional.get();
    }

    public List<TreatmentCategory> getAllCategory() {
       return treatmentCategoryRepository.findAll();
    }
}
