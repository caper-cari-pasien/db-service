package com.caper.dbservice.exceptions.advice;

import com.caper.dbservice.exceptions.DataNotFoundException;
import com.caper.dbservice.exceptions.ErrorTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(value = {DataNotFoundException.class})
    public ResponseEntity<Object> dataNotFound(Exception exception) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        ErrorTemplate baseException = new ErrorTemplate(
                exception.getMessage()
        );

        return new ResponseEntity<>(baseException, status);
    }
}
