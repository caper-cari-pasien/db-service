package com.caper.dbservice.exceptions;

public record ErrorTemplate(String message) {
}
