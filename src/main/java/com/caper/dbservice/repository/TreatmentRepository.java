package com.caper.dbservice.repository;

import com.caper.dbservice.model.Treatment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TreatmentRepository extends JpaRepository<Treatment, String> {
    List<Treatment> findAllByOpenTreatment_Doctor_Id(String doctorId);

    List<Treatment> findAllByPatient_Id(String patientId);

    Optional<Treatment> findById(String id);
}
