package com.caper.dbservice.repository;

import com.caper.dbservice.model.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, String> {
    @Override
    List<Doctor> findAll();

    Optional<Doctor> findById(String id);

    Optional<Doctor> findByUserUsername(String username);
}
