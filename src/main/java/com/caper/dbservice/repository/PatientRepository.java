package com.caper.dbservice.repository;

import com.caper.dbservice.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PatientRepository extends JpaRepository<Patient, String> {
    Optional<Patient> findById(String id);
    Optional<Patient> findByUserUsername(String username);
}
