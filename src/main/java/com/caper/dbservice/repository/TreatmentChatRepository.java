package com.caper.dbservice.repository;
import com.caper.dbservice.model.Treatment;
import com.caper.dbservice.model.TreatmentChat;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TreatmentChatRepository extends JpaRepository<TreatmentChat, String> {

    List<TreatmentChat> findByTreatmentId(String treatmentId);
}