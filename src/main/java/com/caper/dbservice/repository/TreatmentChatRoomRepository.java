package com.caper.dbservice.repository;

import com.caper.dbservice.model.TreatmentChatRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TreatmentChatRoomRepository extends JpaRepository<TreatmentChatRoom, String> {
}
