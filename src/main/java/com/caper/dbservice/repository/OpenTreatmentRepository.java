package com.caper.dbservice.repository;

import com.caper.dbservice.model.OpenTreatment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface OpenTreatmentRepository extends JpaRepository<OpenTreatment, String> {
    @Override
    List<OpenTreatment> findAll();

    Optional<OpenTreatment> findById(String id);
}
