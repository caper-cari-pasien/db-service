package com.caper.dbservice.repository;

import com.caper.dbservice.model.RequestTreatment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RequestTreatmentRepository extends JpaRepository<RequestTreatment, String> {
    @Override
    List<RequestTreatment> findAll();
    Optional<RequestTreatment> findById(String id);
}
