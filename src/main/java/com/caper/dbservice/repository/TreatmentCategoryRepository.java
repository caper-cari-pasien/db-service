package com.caper.dbservice.repository;

import com.caper.dbservice.model.TreatmentCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TreatmentCategoryRepository extends JpaRepository<TreatmentCategory, String> {
    Optional<TreatmentCategory> findById(String id);
}
