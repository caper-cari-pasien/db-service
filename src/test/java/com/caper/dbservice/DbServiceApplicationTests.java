package com.caper.dbservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@SpringBootTest
class DbServiceApplicationTests {

	@Test
	void testApplicationStartsWithoutExceptions() {
		assertDoesNotThrow(() -> DbServiceApplication.main(new String[]{}));
	}
}
