package com.caper.dbservice.controller;

import com.caper.dbservice.dto.DoctorDto;
import com.caper.dbservice.dto.PatientDto;
import com.caper.dbservice.dto.UploadImageUrlDto;
import com.caper.dbservice.model.Doctor;
import com.caper.dbservice.model.Patient;
import com.caper.dbservice.model.User;
import com.caper.dbservice.model.UserRole;
import com.caper.dbservice.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class UserControllerTest {
    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;

    @Test
    void testControllerGetById() {
        // set up
        var userId = "id";
        var user = createUser();

        when(userService.getUserById(userId)).thenReturn(user);

        // test
        var responseEntity = userController.getById(userId);

        // assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(user.getId(), responseEntity.getBody().getId());
        verify(userService, times(1)).getUserById(userId);
    }

    @Test
    void testControllerGetByUsername() {
        // set up
        var username = "username";
        var user = createUser();

        when(userService.getUserByUsername(username)).thenReturn(user);

        // test
        var responseEntity = userController.getByName(username);

        // assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(user.getUsername(), responseEntity.getBody().getUsername());
        verify(userService, times(1)).getUserByUsername(username);
    }

    @Test
    void testControllerAdd() {
        // set up
        var user = createUser();

        when(userService.addUser(user)).thenReturn(user);

        // test
        var responseEntity = userController.add(user);

        // assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(user, responseEntity.getBody());
        verify(userService, times(1)).addUser(user);
    }

    @Test
    void testControllerGetAllDoctor() {
        // set up
        var doctorList = new ArrayList<Doctor>();
        doctorList.add(createDoctor());

        when(userService.getDoctorAll()).thenReturn(doctorList);

        // test
        var responseEntity = userController.getDoctorAll();

        // assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(doctorList, responseEntity.getBody());
        verify(userService, times(1)).getDoctorAll();
    }

    @Test
    void testControllerGetDoctorByUsername() {
        // set up
        var username = "username";
        var doctor = createDoctor();

        when(userService.getDoctorByUsername(username)).thenReturn(doctor);

        // test
        var responseEntity = userController.getDoctorByName(username);

        // assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(doctor, responseEntity.getBody());
        verify(userService, times(1)).getDoctorByUsername(username);
    }

    @Test
    void testControllerGetPatientByUsername() {
        // set up
        var username = "username";
        var patient = createPatient();

        when(userService.getPatientByUsername(username)).thenReturn(patient);

        // test
        var responseEntity = userController.getPatientByName(username);

        // assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(patient, responseEntity.getBody());
        verify(userService, times(1)).getPatientByUsername(username);
    }

    @Test
    void testControllerUpdateDoctor() {
        // set up
        var username = "username";
        var doctor = createDoctor();
        var doctorDto = createDoctorDto();

        when(userService.updateDoctor(username, doctorDto)).thenReturn(doctor);

        // test
        var responseEntity = userController.updateDoctor(username, doctorDto);

        // assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(doctor, responseEntity.getBody());
        verify(userService, times(1)).updateDoctor(username, doctorDto);
    }

    @Test
    void testControllerUpdatePatient() {
        // set up
        var username = "username";
        var patient = createPatient();
        var patientDto = createPatientDto();

        when(userService.updatePatient(username, patientDto)).thenReturn(patient);

        // test
        var responseEntity = userController.updatePatient(username, patientDto);

        // assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(patient, responseEntity.getBody());
        verify(userService, times(1)).updatePatient(username, patientDto);
    }

    @Test
    void testControllerUploadCertificate() {
        // set up
        var username = "username";
        var uploadImageDto = createUploadImageUrlDto();
        var doctor = createDoctor();
        doctor.setCertificateUrl(uploadImageDto.getUrl());

        when(userService.uploadCertificate(username, uploadImageDto)).thenReturn(doctor);

        // test
        var responseEntity = userController.uploadCertificate(username, uploadImageDto);

        // assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(doctor.getCertificateUrl(), responseEntity.getBody().getCertificateUrl());
        verify(userService, times(1)).uploadCertificate(username, uploadImageDto);
    }

    @Test
    void testControllerUploadTeethPicture() {
        // set up
        var username = "username";
        var uploadImageDto = createUploadImageUrlDto();
        var patient = createPatient();
        patient.setTeethPictureUrl(uploadImageDto.getUrl());

        when(userService.uploadTeethPicture(username, uploadImageDto)).thenReturn(patient);

        // test
        var responseEntity = userController.uploadTeethPicture(username, uploadImageDto);

        // assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(patient.getTeethPictureUrl(), responseEntity.getBody().getTeethPictureUrl());
        verify(userService, times(1)).uploadTeethPicture(username, uploadImageDto);
    }


    User createUser() {
        return User.builder().id("id")
                .username("username")
                .domisili("domisili")
                .password("password")
                .build();
    }

    Doctor createDoctor() {
        var user = createUser();
        user.setRole(UserRole.DOCTOR);
        return Doctor.builder().id(user.getId()).user(user).certificateUrl("url").build();
    }

    Patient createPatient() {
        var user = createUser();
        user.setRole(UserRole.PATIENT);
        return Patient.builder().id(user.getId()).user(user).teethPictureUrl("url").build();
    }

    DoctorDto createDoctorDto() {
        var user = createUser();
        return DoctorDto.builder().id(user.getId()).user(user).certificateUrl("url").build();
    }

    PatientDto createPatientDto() {
        var user = createUser();
        return PatientDto.builder().id(user.getId()).user(user).teethPictureUrl("url").build();
    }

    UploadImageUrlDto createUploadImageUrlDto() {
        return UploadImageUrlDto.builder().url("url").build();
    }
}
