package com.caper.dbservice.controller;

import com.caper.dbservice.dto.AddTreatmentNoteDto;
import com.caper.dbservice.dto.RegisterTreatmentDto;
import com.caper.dbservice.model.OpenTreatment;
import com.caper.dbservice.model.Treatment;
import com.caper.dbservice.service.TreatmentBerjalanService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class TreatmentBerjalanControllerTest {
    @Mock
    private TreatmentBerjalanService treatmentBerjalanService;

    @InjectMocks
    private TreatmentBerjalanController treatmentBerjalanController;

    @Test
    void testControllerGetAllByPatientUsername() {
        // set up
        var username = "username";
        var treatmentList = new ArrayList<Treatment>();
        treatmentList.add(createTreatment());

        when(treatmentBerjalanService.findAllTreatmentByPatientUsername(username)).thenReturn(treatmentList);

        // test
        var responseEntity = treatmentBerjalanController.findTreatmentByPatientUsername(username);

        // assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(treatmentList, responseEntity.getBody());
        verify(treatmentBerjalanService, times(1)).findAllTreatmentByPatientUsername(username);
    }

    @Test
    void testControllerGetAllByDoctorUsername() {
        // set up
        var username = "username";
        var treatmentList = new ArrayList<Treatment>();
        treatmentList.add(createTreatment());

        when(treatmentBerjalanService.findAllTreatmentByDoctorUsername(username)).thenReturn(treatmentList);

        // test
        var responseEntity = treatmentBerjalanController.findTreatmentByDoctorUsername(username);

        // assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(treatmentList, responseEntity.getBody());
        verify(treatmentBerjalanService, times(1)).findAllTreatmentByDoctorUsername(username);
    }

    @Test
    void testControllerVerifyTreatment() {
        // set up
        var treatmentId = "id";
        var treatment = createTreatment();
        treatment.setIsVerified(true);

        when(treatmentBerjalanService.verifyTreatment(treatmentId)).thenReturn(treatment);

        // test
        var responseEntity = treatmentBerjalanController.verifyTreatment(treatmentId);

        // assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertTrue(responseEntity.getBody().getIsVerified());
        verify(treatmentBerjalanService, times(1)).verifyTreatment(treatmentId);
    }

    @Test
    void testControllerAddNote() {
        // set up
        var treatmentId = "id";
        var addNoteDto = createAddTreatmentNoteDto();
        var treatment = createTreatment();
        treatment.setNote("note");

        when(treatmentBerjalanService.addNote(treatmentId, addNoteDto)).thenReturn(treatment);

        // test
        var responseEntity = treatmentBerjalanController.addNote(treatmentId, addNoteDto);

        // assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(treatment.getNote(), responseEntity.getBody().getNote());
        verify(treatmentBerjalanService, times(1)).addNote(treatmentId, addNoteDto);
    }

    @Test
    void testControllerRegisterTreatment() {
        // set up
        var registerTreatmentDto = createRegisterTreatmentDto();
        var treatment = createTreatment();

        when(treatmentBerjalanService.registerTreatment(registerTreatmentDto)).thenReturn(treatment);

        // test
        var responseEntity = treatmentBerjalanController.registerTreatment(registerTreatmentDto);

        // assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(treatment, responseEntity.getBody());
        verify(treatmentBerjalanService, times(1)).registerTreatment(registerTreatmentDto);
    }

    @Test
    void testControllerGetTreatmentById() {
        // set up
        var treatmentId = "id";
        var treatment = createTreatment();

        when(treatmentBerjalanService.getTreatmentBerjalanById(treatmentId)).thenReturn(treatment);

        // test
        var responseEntity = treatmentBerjalanController.getTreatmentBerjalanById(treatmentId);

        // assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(treatment, responseEntity.getBody());
        verify(treatmentBerjalanService, times(1)).getTreatmentBerjalanById(treatmentId);
    }

    Treatment createTreatment() {
        return Treatment.builder().id("id").note("").isVerified(false).startTime(Date.from(Instant.now())).build();
    }

    AddTreatmentNoteDto createAddTreatmentNoteDto() {
        return AddTreatmentNoteDto.builder().note("note").build();
    }

    RegisterTreatmentDto createRegisterTreatmentDto() {
        return RegisterTreatmentDto.builder().openTreatmentId("id").username("username").startTime(Date.from(Instant.now())).build();
    }
}
