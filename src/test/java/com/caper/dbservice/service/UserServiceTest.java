package com.caper.dbservice.service;

import com.caper.dbservice.dto.DoctorDto;
import com.caper.dbservice.dto.PatientDto;
import com.caper.dbservice.dto.UploadImageUrlDto;
import com.caper.dbservice.exceptions.DataNotFoundException;
import com.caper.dbservice.model.Doctor;
import com.caper.dbservice.model.Patient;
import com.caper.dbservice.model.User;
import com.caper.dbservice.model.UserRole;
import com.caper.dbservice.repository.DoctorRepository;
import com.caper.dbservice.repository.PatientRepository;
import com.caper.dbservice.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private PatientRepository patientRepository;

    @Mock
    private DoctorRepository doctorRepository;

    @InjectMocks
    private UserService userService;

    @Test
    void testServiceGetUserById() {
        // set up
        var userId = "id";
        var user = createUser();

        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        // test
        var userResult = userService.getUserById(userId);

        assertEquals(user, userResult);
    }

    @Test
    void testServiceGetUserByIdThrowsDataNotFoundException() {
        // set up
        var userId = "id";

        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        // test and assert
        assertThrows(DataNotFoundException.class, () -> userService.getUserById(userId));
    }

    @Test
    void testServiceGetUserByUsername() {
        // set up
        var username = "username";
        var user = createUser();

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));

        // test
        var userResult = userService.getUserByUsername(username);

        assertEquals(user, userResult);
    }

    @Test
    void testServiceGetUserByUsernameThrowsDataNotFoundException() {
        // set up
        var username = "username";

        when(userRepository.findByUsername(username)).thenReturn(Optional.empty());

        // test and assert
        assertThrows(DataNotFoundException.class, () -> userService.getDoctorByUsername(username));
    }

    @Test
    void testServiceAddUserDoctor() {
        // set up
        var user = createUser();
        user.setRole(UserRole.DOCTOR);

        when(userRepository.save(user)).thenReturn(user);
        when(doctorRepository.save(any(Doctor.class))).thenReturn(any(Doctor.class));

        // test
        var savedUser = userService.addUser(user);

        // assert
        assertEquals(user, savedUser);
        verify(userRepository, times(1)).save(user);
        verify(doctorRepository, times(1)).save(any(Doctor.class));
    }

    @Test
    void testServiceAddUserPatient() {
        // set up
        var user = createUser();
        user.setRole(UserRole.PATIENT);

        when(userRepository.save(user)).thenReturn(user);
        when(patientRepository.save(any(Patient.class))).thenReturn(any(Patient.class));

        // test
        var savedUser = userService.addUser(user);

        // assert
        assertEquals(user, savedUser);
        verify(userRepository, times(1)).save(user);
        verify(patientRepository, times(1)).save(any(Patient.class));
    }

    @Test
    void testServiceGetAllDoctor() {
        // set up
        var doctor = createDoctor();
        var doctorList = new ArrayList<Doctor>();
        doctorList.add(doctor);

        when(doctorRepository.findAll()).thenReturn(doctorList);

        // test
        var doctorListResult = userService.getDoctorAll();

        assertEquals(doctorList, doctorListResult);
    }

    @Test
    void testServiceGetAllDoctorThrowsDataNotFoundException() {
        // set up
        var doctorList = new ArrayList<Doctor>();

        when(doctorRepository.findAll()).thenReturn(doctorList);

        // test and assert
        assertThrows(DataNotFoundException.class, () -> userService.getDoctorAll());
    }

    @Test
    void testServiceGetDoctorByUsername() {
        // set up
        var username = "username";
        var user = createUser();
        var doctor = createDoctor();

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
        when(doctorRepository.findById(user.getId())).thenReturn(Optional.of(doctor));

        // test
        var doctorResult = userService.getDoctorByUsername(username);

        // assert
        assertEquals(doctor, doctorResult);
    }

    @Test
    void testServiceGetDoctorByUsernameThrowsDataNotFoundException() {
        // set up
        var username = "username";
        var user = createUser();

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
        when(doctorRepository.findById(user.getId())).thenReturn(Optional.empty());

        // test and assert
        assertThrows(DataNotFoundException.class, () -> userService.getDoctorByUsername(username));
    }

    @Test
    void testServiceGetPatientByUsername() {
        // set up
        var username = "username";
        var user = createUser();
        var patient = createPatient();

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
        when(patientRepository.findById(user.getId())).thenReturn(Optional.of(patient));

        // test
        var patientResult = userService.getPatientByUsername(username);

        // assert
        assertEquals(patient, patientResult);
    }

    @Test
    void testServiceGetPatientByUsernameThrowsDataNotFoundException() {
        // set up
        var username = "username";
        var user = createUser();

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
        when(patientRepository.findById(user.getId())).thenReturn(Optional.empty());

        // test and assert
        assertThrows(DataNotFoundException.class, () -> userService.getPatientByUsername(username));
    }

    @Test
    void testServiceUpdateDoctor() {
        // set up
        var username = "username";
        var user = createUser();
        user.setRole(UserRole.DOCTOR);
        var doctor = createDoctor();
        doctor.setUser(user);
        var doctorDto = createDoctorDto();
        doctorDto.setUser(user);

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
        when(doctorRepository.findById(user.getId())).thenReturn(Optional.of(doctor));
        when(doctorRepository.save(any(Doctor.class))).thenReturn(any(Doctor.class));

        // test
        var doctorResult = userService.updateDoctor(username, doctorDto);

        // assert
        assertEquals(doctor.getUser(), doctorResult.getUser());
    }

    @Test
    void testServiceUpdateDoctorThrowsDataNotFoundException() {
        // set up
        var username = "username";
        var user = createUser();
        user.setRole(UserRole.DOCTOR);
        var doctor = createDoctor();
        doctor.setUser(user);
        var doctorDto = createDoctorDto();
        doctorDto.setUser(user);

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
        when(doctorRepository.findById(user.getId())).thenReturn(Optional.empty());

        // test and assert
        assertThrows(DataNotFoundException.class, () -> userService.updateDoctor(username, doctorDto));
    }

    @Test
    void testServiceUpdatePatient() {
        // set up
        var username = "username";
        var user = createUser();
        user.setRole(UserRole.PATIENT);
        var patient = createPatient();
        patient.setUser(user);
        var patientDto = createPatientDto();
        patientDto.setUser(user);

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
        when(patientRepository.findById(user.getId())).thenReturn(Optional.of(patient));
        when(patientRepository.save(any(Patient.class))).thenReturn(any(Patient.class));

        // test
        var patientResult = userService.updatePatient(username, patientDto);

        // assert
        assertEquals(patient.getUser(), patientResult.getUser());
    }

    @Test
    void testServiceUpdatePatientThrowsDataNotFoundException() {
        // set up
        var username = "username";
        var user = createUser();
        user.setRole(UserRole.PATIENT);
        var patient = createPatient();
        patient.setUser(user);
        var patientDto = createPatientDto();
        patientDto.setUser(user);

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
        when(patientRepository.findById(user.getId())).thenReturn(Optional.empty());

        // test and assert
        assertThrows(DataNotFoundException.class, () -> userService.updatePatient(username, patientDto));
    }

    @Test
    void testServiceUpdateUser() {
        // set up
        var user = createUser();

        when(userRepository.findByUsername(user.getUsername())).thenReturn(Optional.of(user));

        // test
        var userResult = userService.updateUser(user);

        // assert
        assertEquals(user, userResult);
    }

    @Test
    void testServiceUpdateUserThrowsDataNotFoundException() {
        // set up
        var user = createUser();

        when(userRepository.findByUsername(user.getUsername())).thenReturn(Optional.empty());

        // test and assert
        assertThrows(DataNotFoundException.class, () -> userService.updateUser(user));
    }

    @Test
    void testServiceUploadCertificate() {
        // set up
        var username = "username";
        var uploadImageDto = createUploadImageUrlDto();
        var doctor = createDoctor();

        when(doctorRepository.findByUserUsername(username)).thenReturn(Optional.of(doctor));

        // test
        var doctorResult = userService.uploadCertificate(username, uploadImageDto);

        // assert
        assertEquals(uploadImageDto.getUrl(), doctorResult.getCertificateUrl());
    }

    @Test
    void testServiceUploadCertificateThrowsDataNotFoundException() {
        // set up
        var username = "username";
        var uploadImageDto = createUploadImageUrlDto();

        when(doctorRepository.findByUserUsername(username)).thenReturn(Optional.empty());

        // test and assert
        assertThrows(DataNotFoundException.class, () -> userService.uploadCertificate(username, uploadImageDto));
    }

    @Test
    void testServiceUploadTeethPicture() {
        // set up
        var username = "username";
        var uploadImageDto = createUploadImageUrlDto();
        var patient = createPatient();

        when(patientRepository.findByUserUsername(username)).thenReturn(Optional.of(patient));

        // test
        var patientResult = userService.uploadTeethPicture(username, uploadImageDto);

        // assert
        assertEquals(uploadImageDto.getUrl(), patientResult.getTeethPictureUrl());
    }

    @Test
    void testServiceUploadTeethPictureThrowsDataNotFoundException() {
        // set up
        var username = "username";
        var uploadImageDto = createUploadImageUrlDto();

        when(patientRepository.findByUserUsername(username)).thenReturn(Optional.empty());

        // test and assert
        assertThrows(DataNotFoundException.class, () -> userService.uploadTeethPicture(username, uploadImageDto));
    }

    User createUser() {
        return User.builder().id("id")
                .username("username")
                .domisili("domisili")
                .password("password")
                .build();
    }

    Doctor createDoctor() {
        var user = createUser();
        user.setRole(UserRole.DOCTOR);
        return Doctor.builder().id(user.getId()).user(user).certificateUrl("url").build();
    }

    Patient createPatient() {
        var user = createUser();
        user.setRole(UserRole.PATIENT);
        return Patient.builder().id(user.getId()).user(user).teethPictureUrl("url").build();
    }

    DoctorDto createDoctorDto() {
        var user = createUser();
        return DoctorDto.builder().id(user.getId()).user(user).certificateUrl("url").build();
    }

    PatientDto createPatientDto() {
        var user = createUser();
        return PatientDto.builder().id(user.getId()).user(user).teethPictureUrl("url").build();
    }

    UploadImageUrlDto createUploadImageUrlDto() {
        return UploadImageUrlDto.builder().url("url").build();
    }
}
